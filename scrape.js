const axios = require('axios');
const cheerio = require('cheerio');
const fs = require('fs');
var date = new Date();
const writeStream = fs.createWriteStream(`quotes-${date.getMilliseconds()}.html`);

/*
    description: scrapes office quotes from officequotes.net
    purpose: thinking of making an office quote generator. 
    commnents: I guess I could just send a request to a random season and episode and then take random quote from the page instead.    
*/

async function scrape() {    
    var seasons = 9;    
    writeStream.write(`<div> \n`);
    writeStream.write(`<h1>Office quotes: </h1> \n`);
    for (var i = 1; i < seasons + 1; i++) {
        var season = i;
        var episode = 1;
        var valid = true;
        writeStream.write(`<h4>Season: ${season} </h4> \n`);
        while (valid) {
            try {
                var response = await axios(`http://officequotes.net/no${season}-${episode < 10 ? '0' + episode : episode}.php`);            
                if (!response.error && response.status == 200) {
                    const $ = cheerio.load(response.data);
                    writeStream.write(`<h5 class="episode-${episode}">Episode: ${episode} </h5> \n`);
                    $('.quote').each((i, el) => {
                        const content = $(el).contents();
                        writeStream.write(`<div class="quote-${i+1}"></h6>\n ${content}\n</div>\n`);
                    });
                    console.log(`Finished episode no. ${episode}`);
                } else {
                    console.log(`Finished season no. ${season}`);
                    valid = false;
                }
                episode++;
            } catch(error){
                valid = false;
            }
        }
        console.log('Scraping Done...');
    }
    writeStream.write(`</div>`);
}
scrape();



